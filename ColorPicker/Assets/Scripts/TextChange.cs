using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using FatherTime;
using UnityEngine.SceneManagement;


//using namespace just in case I might want to use in another script!
namespace Main
{
    public class TextChange : MonoBehaviour
    {
        public string currentColor;
        private int previousNumber;
        private int index;
        protected float score = 0f;
        private int colorsRemaining = 20;

        public TMP_Text colorText;
        public TMP_Text scoreCounter;
        public TMP_Text colorCounter;
        public Timer time;

        [SerializeField] Button[] buttonChoice;

        //declare each color to be changed!
        public Color colorRed;
        public Color colorBlue;
        public Color colorYellow;
        public Color colorPink;



        void Start()
        {
            SetRandomColor();
            SetRandomColorName();
            scoreCounter.text = score.ToString();
            colorCounter.text = colorsRemaining.ToString();

            foreach (Button button in buttonChoice)
            {
                Button choice = button;
                button.onClick.AddListener(() => OnClick(choice));
                SetRandomColor();
                SetRandomColorName();
            }

        }

        void Update()
        {
            if (colorsRemaining == 0)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex  + 1);

            }
            else if (time.timeRemaining == 0)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            }

        }
       
        //button on click for the game
        public void OnClick(Button choice)
        {

            if (choice.gameObject.tag == currentColor)
            {
                time.timeRemaining = 3f;
                score += 1 * time.timeRemaining;

                //had to do a round here so that only one decimal is shown instead of a lot.
                score = Mathf.Round(score * 10.0f) * 0.1f; 
                scoreCounter.text = score.ToString();
                ScoreKeeper.currentScore = score;
                colorsRemaining--;
                colorCounter.text = colorsRemaining.ToString();
                SetRandomColor();
                SetRandomColorName();
               

            }
            else
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);

            }
        }

        void SetRandomColor()
        {
            //starts with a random range of the 4 colors
            index = Random.Range(0, 4);

            /////////////////////////////////////////////////////////////////////////////////////////////////////////
            //Tried to do a line of code here where the color doesn't repeat from the previous one, sadly not done!//
            /////////////////////////////////////////////////////////////////////////////////////////////////////////
            //int[] colorArray = { 0, 1, 2, 3 };


            /*while (previousNumber == index)
            {
                index = previousNumber;
            }
            previousNumber = index;

            for (int i = 0; i < colorArray.Length; i++)
            {
                int temp = colorArray[i];
                int n = Random.Range(0, colorArray.Length);
                colorArray[i] = colorArray[n];
                colorArray[n] = temp;
            }*/

            
            //This switch case uses the index above to indicate what random color is to be used
            switch (index)
            {

                case 0:
                    currentColor = "Red";
                    colorText.color = colorRed;
                    break;
                case 1:
                    currentColor = "Blue";
                    colorText.color = colorBlue;
                    break;
                case 2:
                    currentColor = "Yellow";
                    colorText.color = colorYellow;
                    break;
                case 3:
                    currentColor = "Pink";
                    colorText.color = colorPink;
                    break;

            }


        }


        void SetRandomColorName()
        {

            //starting with a random color name!
            index = Random.Range(0, 4);


            //Same with the above tried to implement something so that the random does not repeat the previous

            /*int[] colorNameArray = { 0, 1, 2, 3 };

            while (previousNumber == index)
            {
                index = previousNumber;
            }
            previousNumber = index;

            for (int i = 0; i < colorNameArray.Length; i++)
            {
                int temp = colorNameArray[i];
                int n = Random.Range(0, colorNameArray.Length);
                colorNameArray[i] = colorNameArray[n];
                colorNameArray[n] = temp;
            }*/

            //This switch case uses the index above to indicate what random color name to be used instead!
            switch (index)
            {
                case 0:
                    colorText.text = "Red";
                    break;
                case 1:
                    colorText.text = "Blue";
                    break;
                case 2:
                    colorText.text = "Yellow";
                    break;
                case 3:
                    colorText.text = "Pink";
                    break;
            }
        }

        public void BackToMain()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);

        }
    }

}
