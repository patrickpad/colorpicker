using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace FatherTime
{

    public class Timer : MonoBehaviour
    {

        public float timeRemaining;
        public float timerMax;
        public Slider timeSlider;
        public float timeMultiplier = 1.0f;

        void Start()
        {
            timeRemaining = timerMax;
            timeMultiplier = 1.0f;
        }
        void Update()
        {
            timeSlider.value = SliderValue();


            if (timeRemaining <= 0)
            {
                timeRemaining = 0;
            }
            else if (timeRemaining > 0)
            {
                timeRemaining -= Time.deltaTime;
                timeMultiplier -= Time.deltaTime;
            }
        }


        float SliderValue()
        {
            return (timeRemaining / timerMax);
        }

        float TimeMultiplier()
        {
            return (timeMultiplier / timerMax);
        }
    }

}
