using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using FatherTime;
using Main;
using UnityEngine.SceneManagement;


public class ScoreScreen : TextChange
{
    //this script uses the script ScoreKeeper where the scores are stored


    public TMP_Text highScore;
    public TMP_Text screenScore;
    
    void Start()
    {
        screenScore.text = ScoreKeeper.currentScore.ToString();
        highScore.text = ScoreKeeper.highScore.ToString();

    }

    void Update()
    {
        screenScore.text = ScoreKeeper.currentScore.ToString();

        //if the current score of the user is higher than the current high score then make that the high score!
        if (ScoreKeeper.currentScore > ScoreKeeper.highScore)
        {
            ScoreKeeper.highScore = ScoreKeeper.currentScore;
            highScore.text = ScoreKeeper.highScore.ToString();
        }
        
    }


    //Scene Management for buttons
    public void PlayAgain()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }

    public void MainMenu()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 2);
    }

}
