using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class MusicScript : MonoBehaviour
{
    private AudioSource audioSrce;
    public static MusicScript instance;

    private void Awake()
    {

        //if this is the only instance of the object then keep playing music if not then destroy this object
        //makes it so that no two versions of the same object is playing and ensures that there is only one.
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
        
    }

    public void PlayMusic()
    {
        audioSrce = GetComponent<AudioSource>();
        if (!audioSrce.isPlaying)
        {
            return;
        }
        audioSrce.Play();
    }


}
